package ru.syskaev.edu.geekbrains.android.level1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String LANG_TAG = "languageToLoad";

    private String languageToLoad = "en";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLanguage();
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.language) {
            changeLanguage();
            return true;
        }
        else if (id == R.id.info) {
            startInfoActivity();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void setLanguage() {
        languageToLoad = getSharedPreferences(LANG_TAG, MODE_PRIVATE)
                .getString(LANG_TAG, "en");
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    private void changeLanguage() {
        if(languageToLoad.contains("en"))
            languageToLoad = "ru";
        else
            languageToLoad = "en";
        SharedPreferences languagepref = getSharedPreferences(LANG_TAG, MODE_PRIVATE);
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString(LANG_TAG, languageToLoad);
        editor.commit();
        Intent refresh = new Intent(this, this.getClass());
        refresh.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(refresh);
    }

    private void startInfoActivity() {
        Intent info = new Intent(this, InfoActivity.class);
        startActivity(info);
    }

}
