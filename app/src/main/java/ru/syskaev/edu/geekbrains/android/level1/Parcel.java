package ru.syskaev.edu.geekbrains.android.level1;

import java.io.Serializable;

public class Parcel implements Serializable {
    private int index;
    private String cityName;

    public int getIndex() {
        return index;
    }

    public String getCityName() {
        return cityName;
    }

    public Parcel(int index, String cityName) {
        this.index = index;
        this.cityName = cityName;
    }
}
