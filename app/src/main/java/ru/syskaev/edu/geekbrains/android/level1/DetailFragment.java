package ru.syskaev.edu.geekbrains.android.level1;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class DetailFragment extends Fragment {

    public static final String PARCEL = "parcel";
    private static final String CITY_INDEX_KEY = "city_index";
    private static final int MAX_ABS_TEMPERATURE = 40;
    private static final int WEATHER_ICONS_ATLAS_N = 4;
    private static final int WEATHER_ICONS_ATLAS_M = 3;

    Random random = new Random();
    Bitmap[][] weatherIcons = new Bitmap[WEATHER_ICONS_ATLAS_N][WEATHER_ICONS_ATLAS_M];

    public static DetailFragment create(Parcel parcel) {
        DetailFragment f = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(PARCEL, parcel);
        f.setArguments(args);

        return f;
    }

    public Parcel getParcel() {
        Parcel parcel = (Parcel) getArguments().getSerializable(PARCEL);
        return parcel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_detail, container, false);

        prepareWeatherIcons();
        prepareHistoryButton(layout);

        ImageView weatherIcon = (ImageView) layout.findViewById(R.id.weatherIcon);
        weatherIcon.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim);
            v.startAnimation(animation);
        });

        TextView cityName = (TextView) layout.findViewById(R.id.cityName);

        TextView detailText1 = (TextView) layout.findViewById(R.id.detail1Value);
        TextView detailText2 = (TextView) layout.findViewById(R.id.detail2Value);
        TextView detailText3 = (TextView) layout.findViewById(R.id.detail3Value);
        TextView detailText4 = (TextView) layout.findViewById(R.id.detail4Value);
        TextView detailText5 = (TextView) layout.findViewById(R.id.detail5Value);

        weatherIcon.setImageBitmap(getRandomWeatherIcon());
        if (getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE)
            weatherIcon.setVisibility(View.INVISIBLE);

        Parcel parcel = getParcel();
        cityName.setText(parcel.getCityName());

        String value = getRandomTemperatureValueAsString();
        detailText1.setText(value);
        CitiesHistory.setValueByCityIndex(Integer.parseInt(value), parcel.getIndex());

        detailText2.setText(getRandomTemperatureValueAsString());
        detailText3.setText(getRandomTemperatureValueAsString());
        detailText4.setText(getRandomTemperatureValueAsString());
        detailText5.setText(getRandomTemperatureValueAsString());
        return layout;
    }

    private void prepareHistoryButton(View layout) {
        Button historyButton = layout.findViewById(R.id.buttonHistory);
        historyButton.setOnClickListener((view) -> {
            Intent history = new Intent(view.getContext(), HistoryActivity.class);
            history.putExtra(CITY_INDEX_KEY, getParcel().getIndex());
            startActivity(history);
        });
    }

    private String getRandomTemperatureValueAsString() {
        int val = random.nextInt(2 * MAX_ABS_TEMPERATURE) - MAX_ABS_TEMPERATURE;
        return String.valueOf(val);
    }

    private void prepareWeatherIcons() {
        Bitmap atlas = BitmapFactory.decodeResource(getResources(), R.drawable.weather_icons);
        int width, height;
        width = atlas.getWidth() / WEATHER_ICONS_ATLAS_N;
        height = atlas.getHeight() / WEATHER_ICONS_ATLAS_M;
        for(int n = 0; n < WEATHER_ICONS_ATLAS_N; n++) {
            for(int m = 0; m < WEATHER_ICONS_ATLAS_M; m++) {
                weatherIcons[n][m] = Bitmap.createBitmap(atlas, n * width, m * height, width, height);
            }
        }
    }

    private Bitmap getRandomWeatherIcon() {
        int n = random.nextInt(WEATHER_ICONS_ATLAS_N), m = random.nextInt(WEATHER_ICONS_ATLAS_M);
        return weatherIcons[n][m];
    }

}
