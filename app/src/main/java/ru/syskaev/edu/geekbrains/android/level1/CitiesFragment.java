package ru.syskaev.edu.geekbrains.android.level1;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import static ru.syskaev.edu.geekbrains.android.level1.DetailFragment.PARCEL;

public class CitiesFragment extends Fragment {

    boolean isExistDetail;
    Parcel currentParcel;
    RecyclerView recyclerView;
    CitiesAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = view.findViewById(R.id.cities_container);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null)
            currentParcel = (Parcel) savedInstanceState.getSerializable("CurrentCity");
        else
            currentParcel = new Parcel(0, getResources().getTextArray(R.array.Cities)[0].toString());

        List<String> cities = Arrays.asList(getResources().getStringArray(R.array.Cities));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        citiesAdapter = new CitiesAdapter(cities, this);
        recyclerView.setAdapter(citiesAdapter);

        View detailsFrame = getActivity().findViewById(R.id.detail_fragment);
        isExistDetail = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (isExistDetail){
            showDetail(currentParcel);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("CurrentCity", currentParcel);
    }

    public void onItemClick(View v, int position) {
        TextView cityNameView = (TextView) v;
        currentParcel =  new Parcel(position, cityNameView.getText().toString());
        citiesAdapter.setCurrentCityIndex(position);
        recyclerView.refreshDrawableState();
        showDetail(currentParcel);
    }

    @Override
    public void onStart() {
        super.onStart();
        recyclerView.scrollToPosition(currentParcel.getIndex());
    }

    private void showDetail(Parcel parcel) {
        if (isExistDetail) {

            DetailFragment detail = (DetailFragment)
                    getFragmentManager().findFragmentById(R.id.detail_fragment);

            if (detail == null || detail.getParcel().getIndex() != parcel.getIndex()) {

                detail = DetailFragment.create(parcel);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.detail_fragment, detail);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }

        }
        else {
            Intent intent = new Intent();
            intent.setClass(getActivity(), DetailActivity.class);
            intent.putExtra(PARCEL, parcel);
            startActivity(intent);
        }
    }

}
